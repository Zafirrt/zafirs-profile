from django.db import models

class Message(models.Model):
	senderName = models.CharField(max_length=50)
	senderEmail = models.EmailField()
	senderMessage = models.TextField()

class User(models.Model):
	username = models.CharField(max_length=20)
	email = models.EmailField()
	password = models.CharField(max_length=100)

class Project(models.Model):
	projectTitle = models.CharField(max_length=100)
	projectDate = models.DateField()
	projectDescription = models.TextField()
	projectPlace = models.CharField(max_length=50)
	projectCategory = models.CharField(max_length=50)