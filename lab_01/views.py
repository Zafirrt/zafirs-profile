from django.shortcuts import render
from django.utils import timezone
from django.http import HttpResponseRedirect
import datetime
from .forms import *
from .models import *
from django.contrib.auth.hashers import make_password
import pytz

timezone = pytz.timezone("Etc/GMT+7")
now = timezone.localize(datetime.datetime.now())
context = {'now': now}

def index(request):
	return render(request, 'index.html', {'index_active': 'active', 'context': context})

def feedback(request):
	if request.method == 'POST':
		response = {}
		form = FeedbackForm(request.POST)
		if form.is_valid():
			response['name'] 		= form.cleaned_data['name']
			response['email'] 	= request.POST['email']
			response['message'] 	= form.cleaned_data['message']
			message = Message(
					senderName		= response['name'], 
					senderEmail		= response['email'], 
					senderMessage	= response['message'], 
				)
			message.save()
			return HttpResponseRedirect('/projects/thanks/')
	else:
		form = FeedbackForm()
	feed_context = {'feedback_active': 'active', 'context': context, 'message_form': form}
	return render(request, 'feedback.html', feed_context)

def signup(request):
	if request.method == 'POST':
		response = {}
		form = UserForm(request.POST)
		if form.is_valid():
			response['username'] = form.cleaned_data['username']
			response['email'] 	= form.cleaned_data['email']
			response['password'] = form.cleaned_data['password']
			user = User(
					username			= response['username'], 
					email 			= response['email'], 
					password 		= make_password(response['password']), 
				)
			user.save()
			return HttpResponseRedirect('/')
	else:
		form = UserForm()
	signup_context = {'signup_active': 'active', 'context': context, 'signup_form': form}
	return render(request, 'signup.html', signup_context)

def projectInput(request):
	if request.method == 'POST':
		response = {}
		form = ProjectsForm(request.POST)
		if form.is_valid():
			response['title'] 			= form.cleaned_data['title']
			response['date'] 				= request.POST['date']
			response['description'] 	= form.cleaned_data['description']
			response['place'] 			= form.cleaned_data['place']
			response['category'] 		= form.cleaned_data['category']
			project = Project(
					projectTitle			= response['title'], 
					projectDate				= response['date'], 
					projectDescription	= response['description'], 
					projectPlace			= response['place'], 
					projectCategory		= response['category'], 
				)
			project.save()
			return HttpResponseRedirect('/projects/thanks/')
	else:
		form = ProjectsForm()
	projectInput_context = {'projects_active': 'active', 'context': context, 'projectInputForm': form}
	return render(request, 'projectInput.html', projectInput_context)

def projects(request):
	projects_context = {'projects_active': 'active', 'context': context, 'projects': Project.objects.all().values()}
	return render(request, 'projects.html', projects_context)

def projectThanks(request):
	projectThanks_context = {'projects_active': 'active', 'context': context}
	return render(request, 'projectThanks.html', projectThanks_context)
