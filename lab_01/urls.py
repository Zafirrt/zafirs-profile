from django.urls import path
from . import views

urlpatterns = [
		path('', views.index, name='index'),
		path('feedback/', views.feedback, name='feedback'),
		path('signup/', views.signup, name='signup'),
		path('projects/', views.projects, name='projects'),
		path('projects/input/', views.projectInput, name='projectInput'),
		path('projects/thanks/', views.projectThanks, name='projectThanks')
]