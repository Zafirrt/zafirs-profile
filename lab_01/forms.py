from django import forms
from .models import *
import datetime

class FeedbackForm(forms.Form):
	mainattrs = {'class': 'feed-input-text'}
	name = forms.CharField(
		label='Name', 
		required=True, 
		max_length=50, 
		widget=forms.TextInput(attrs=mainattrs)
	)
	email = forms.EmailField(
		required=True, 
		widget=forms.EmailInput(attrs=mainattrs)
	)
	message = forms.CharField(
		required=True, 
		max_length=300, 
		widget=forms.Textarea()
	)

class UserForm(forms.ModelForm):
	mainattrs = {'class': 'sign-input-text'};
	username = forms.CharField(max_length=20, widget=forms.TextInput(attrs=mainattrs))
	email = forms.EmailField(widget=forms.EmailInput(attrs=mainattrs))
	password = forms.CharField(min_length=8, widget=forms.PasswordInput(attrs=mainattrs))
	repassword = forms.CharField(min_length=8, widget=forms.PasswordInput(attrs=mainattrs))
	class Meta:
		model = User
		fields=('username', 'email', 'password')
	def clean(self):
		cleaned_data = super(UserForm, self).clean()
		password = cleaned_data.get("password")
		confirm_password = cleaned_data.get("repassword")

		if password != confirm_password:
			raise forms.ValidationError(
				"Passwords do not match!"
			)


class ProjectsForm(forms.Form):
   mainattrs = {'class': 'project-input-text'}
   title = forms.CharField(
   	label='Project Title', 
   	required=True, 
   	max_length=100, 
   	widget=forms.TextInput(attrs=mainattrs)
   )
   date = forms.CharField(
   	label='Date', 
   	required=True,
   	max_length=10, 
   	widget=forms.DateInput(format='%d/%m/%Y', attrs=mainattrs)
   )
   description = forms.CharField(
   	label='Description', 
   	required=False, 
   	max_length=100, 
  		empty_value="",
   	widget=forms.Textarea(attrs={'class': 'project-input-text', 'style': 'color: white'})
   )
   #fix later
   place = forms.CharField(
   	label='Place', 
   	required=True, 
   	max_length=100, 
   	widget=forms.TextInput(attrs=mainattrs)
   )
   category = forms.CharField(
   	label='Category', 
   	required=False, 
   	max_length=100, 
   	empty_value='None', 
   	widget=forms.TextInput(attrs=mainattrs)
   )

        