# Generated by Django 2.1.1 on 2019-03-08 13:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_01', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=254)),
                ('password', models.CharField(max_length=100)),
            ],
        ),
        migrations.AlterField(
            model_name='message',
            name='senderMessage',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='project',
            name='projectCategory',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='project',
            name='projectDescription',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='project',
            name='projectPlace',
            field=models.CharField(max_length=50),
        ),
    ]
